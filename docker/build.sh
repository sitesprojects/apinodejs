APP_NAME=$1
APP_TAG=$2

git pull origin master

rm -rfv src

cp -r ../src .

echo "## Generando imagen $APP_NAME:$APP_TAG#"

docker build . -t $APP_NAME:$APP_TAG
