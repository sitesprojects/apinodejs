var appRouter = function(app) {

    app.get("/", function(req, res) {
        var response = {
            "success": "OK",
            "error": "",
            "data": []
        }
        res.send(response);
    });

}

module.exports = appRouter;
